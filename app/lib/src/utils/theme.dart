import 'package:flutter/material.dart';

ThemeData light = ThemeData(
    brightness: Brightness.light,
    primarySwatch: Colors.white,
    accentColor: Colors.grey,
    scaffoldBackgroundColor: Colors.white);

ThemeData dark = ThemeData(
    brightness: Brightness.dark,
    primarySwatch: Colors.black87,
    accentColor: Colors.grey[600],
    scaffoldBackgroundColor: Colors.blueAccent);
