import 'dart:async';

import 'package:light/light.dart';

class luxDetector {
  String _luxString = "unknown";
  StreamSubscription _subscription;

  bool toggle = true;

  void onData(int luxValue) async {
    print("lux value: $luxValue");
  }

  void stopListening() {
    _subscription.cancel();
  }

  void startListening() {
    Light _light = new Light();
    try {
      _subscription = _light.lightSensorStream.listen(onData);
    } on LightException catch (exception) {
      print(exception);
    }
  }
}
