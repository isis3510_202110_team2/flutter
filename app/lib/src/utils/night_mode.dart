import 'package:flutter/cupertino.dart';

class nightModeNotifier extends ChangeNotifier {
  bool _nightMode;

  bool get nightMode => _nightMode;

  nightModeNotifier() {
    _nightMode = false;
  }

  toglemode() {
    _nightMode = !_nightMode;
    notifyListeners();
  }
}

bool isNightMode() {
  // bool nightMode;
  // var now = TimeOfDay.now();
  // if (now.hour > 18 || now.hour < 6) {
  //   nightMode = true;
  // } else {
  //   nightMode = false;
  // }
  return false;
}
