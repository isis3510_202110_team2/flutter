import 'dart:async';

import 'package:app/src/utils/checkConnection.dart';
import 'package:app/src/widgets/toast_alert.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:light/light.dart';

import './user/home.dart';
import '../bloc/bloc_provider.dart';
import '../bloc/blocs/login_bloc.dart';
import '../utils/night_mode.dart';
import '../utils/notification_dialog.dart';
import '../widgets/button.dart';
import '../widgets/spinner.dart';

class LoginScreen extends StatefulWidget {
  final FirebaseAnalytics analytics;
  final FirebaseAnalyticsObserver observer;

  LoginScreen({this.analytics, this.observer});

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  //light sensor dark mode code --------------------------------------
  Light _light;
  bool _dark = false;
  StreamSubscription _subscription;

  void onData(int luxValue) async {
    print("lux value: $luxValue");
    setState(() {
      _dark = luxValue <= 10 ? true : false;
    });
  }

  void stopListening() {
    _subscription.cancel();
  }

  void startListening() {
    _light = new Light();
    try {
      _subscription = _light.lightSensorStream.listen(onData);
    } on LightException catch (e) {
      print(e);
    }
  }

  Future<void> initPlatFormState() async {
    startListening();
  }

  //End of light sensor dark mode code ---------------------------------
  Future<void> _logLogin() async {
    await widget.analytics.logLogin();
  }

  Future<void> _setCurrentScreen() async {
    await widget.analytics.setCurrentScreen(screenName: "Login");
  }

  //Important to start the sensor read light information
  @override
  void initState() {
    super.initState();
    initPlatFormState();
    _setCurrentScreen();
  }

  @override
  Widget build(BuildContext context) {
    bool nightMode = isNightMode();
    final bloc = Provider.of<LoginBloc>(context);
    final size = MediaQuery.of(context).size;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        //custom app bar with own implementation of back button
        //to stop the stream of information from light sensor
        elevation: 0.0,
        backgroundColor: _dark ? Colors.black : Colors.white,
        leading: IconButton(
          icon: Icon(Icons.arrow_back,
              color: _dark ? Colors.white : Colors.black),
          onPressed: () {
            Navigator.of(context).pop();
            stopListening();
          },
        ),
      ),
      body: Container(
        alignment: Alignment.centerLeft,
        color: _dark ? Colors.black : Colors.white,
        child: Column(
          children: [
            Text(
              "Login",
              style: Theme.of(context).textTheme.headline4.copyWith(
                    color: _dark ? Colors.white : Colors.black,
                  ),
            ),
            Container(
                margin: EdgeInsets.symmetric(vertical: 10.0),
                padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
                child: StreamBuilder(
                  stream: bloc.loginEmail,
                  builder: (streamContext, snapshot) {
                    return TextField(
                      onChanged: bloc.changeLoginEmail,
                      keyboardType: TextInputType.emailAddress,
                      cursorColor: Colors.black,
                      style: TextStyle(
                        color: Colors.black,
                      ),
                      decoration: InputDecoration(
                          hintText: "example@mail.com",
                          errorText: snapshot.error),
                    );
                  },
                )),
            Container(
                margin: EdgeInsets.symmetric(vertical: 10.0),
                padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
                child: StreamBuilder(
                  stream: bloc.loginPassword,
                  builder: (streamContext, snapshot) {
                    return TextField(
                      onChanged: bloc.changeLoginPassword,
                      cursorColor: Colors.black,
                      style: TextStyle(
                        color: Colors.black,
                      ),
                      obscureText: true,
                      decoration: InputDecoration(
                          hintText: "password", errorText: snapshot.error),
                    );
                  },
                )),
            Container(
              margin: EdgeInsets.symmetric(vertical: 8.0),
              padding: EdgeInsets.symmetric(horizontal: 20.0),
              child: StreamBuilder(
                stream: bloc.loginSubmit,
                builder: (streamContext, snapshot) {
                  return AppButton(
                    color: Theme.of(context).colorScheme.primary,
                    text: "Log In",
                    onPressed: snapshot.hasData
                        ? () async {
                            bool connected = await checkConnectivity();
                            if (connected) {
                              dialog(context, content: LoadingSpinner());
                              try {
                                var blocData = await bloc.login();

                                Navigator.pop(streamContext);
                                if (!blocData["verified"]) {
                                  showToast(
                                      "You are not verified, please go check your email",
                                      context);
                                }

                                //_logLogin();

                                Navigator.pushReplacement(
                                    streamContext,
                                    MaterialPageRoute(
                                        builder: (_) => HomeScreen()));
                                stopListening();
                              } catch (error) {
                                Navigator.pop(streamContext);
                                Fluttertoast.showToast(
                                  msg: error,
                                  toastLength: Toast.LENGTH_LONG,
                                  gravity: ToastGravity.BOTTOM,
                                  textColor: Colors.white,
                                  backgroundColor:
                                      Theme.of(context).colorScheme.primary,
                                );
                                return;
                              }
                            } else {
                              showDialog(
                                barrierDismissible: false,
                                context: context,
                                builder: (context) => AlertDialog(
                                  backgroundColor:
                                      _dark ? Colors.black : Colors.white,
                                  title: Text(
                                    "No internet connection",
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline4
                                        .copyWith(
                                            color: _dark
                                                ? Colors.white
                                                : Colors.black,
                                            fontWeight: FontWeight.bold),
                                  ),
                                  content: Text(
                                    'Keep in mind you don\'t have an internet connection at the moment, only last online session credentials are valid.',
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline6
                                        .copyWith(
                                          color: _dark
                                              ? Colors.white
                                              : Colors.black,
                                        ),
                                  ),
                                  actions: <Widget>[
                                    TextButton(
                                      onPressed: () async {
                                        //Method to login while offline
                                        String offlineLogResult =
                                            await bloc.offlineLogin();

                                        Navigator.of(context).pop();
                                        if (offlineLogResult == "valid") {
                                          showToast(
                                              "You are logged without internet connection",
                                              context);
                                          Navigator.pushReplacement(
                                              streamContext,
                                              MaterialPageRoute(
                                                  builder: (_) =>
                                                      HomeScreen()));
                                        } else {
                                          showToast(
                                              "Wrong email or password from last saved session",
                                              context);
                                        }
                                      },
                                      child: Text(
                                        'Log in',
                                        textScaleFactor: 1.2,
                                      ),
                                    ),
                                    TextButton(
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                      },
                                      child: Text(
                                        'Cancel',
                                        textScaleFactor: 1.2,
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            }
                          }
                        : null,
                    minWidth: size.width,
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
