import 'dart:async';
import 'dart:io';

import 'package:app/src/bloc/bloc_provider.dart';
import 'package:app/src/bloc/blocs/pet/create_pet_bloc.dart';
import 'package:app/src/utils/checkConnection.dart';
import 'package:app/src/utils/permissions.dart';
import 'package:app/src/widgets/spinner.dart';
import 'package:app/src/widgets/toast_alert.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';
import 'package:light/light.dart';

import '../../utils/night_mode.dart';
import '../../utils/notification_dialog.dart';

class PetRegisterScreen extends StatefulWidget {
  final FirebaseAnalytics analytics;
  final FirebaseAnalyticsObserver observer;

  PetRegisterScreen({this.analytics, this.observer});

  @override
  _PetRegisterScreenState createState() => _PetRegisterScreenState();
}

class _PetRegisterScreenState extends State<PetRegisterScreen> {
  //light sensor dark mode code --------------------------------------
  Light _light;
  bool _dark = false;
  StreamSubscription _subscription;

  void onData(int luxValue) async {
    print("lux value: $luxValue");
    setState(() {
      _dark = luxValue <= 10 ? true : false;
    });
  }

  void stopListening() {
    _subscription.cancel();
  }

  void startListening() {
    _light = new Light();
    try {
      _subscription = _light.lightSensorStream.listen(onData);
    } on LightException catch (e) {
      print(e);
    }
  }

  Future<void> initPlatFormState() async {
    startListening();
  }

  //End of light sensor dark mode code ---------------------------------

  String date;
  File picture;

  Future<void> _setCurrentScreen() async {
    await widget.analytics.setCurrentScreen(screenName: "PetRegister");
  }

  Future<void> _sendEvent() async {
    await widget.analytics.logEvent(name: "pet_registration");
  }

  //Important to start the sensor read light information
  @override
  void initState() {
    super.initState();
    initPlatFormState();
    _setCurrentScreen();
  }

  @override
  Widget build(BuildContext context) {
    final bloc = Provider.of<CreatePetBloc>(context);
    bool nightMode = isNightMode();
    final size = MediaQuery.of(context).size;
    String imageUrl = "assets/icons/image.png";

    return Scaffold(
      backgroundColor: _dark ? Color(0xff3a6a8c) : Color(0xff99b2dd),
      appBar: AppBar(
        backgroundColor: _dark ? Color(0xff3a6a8c) : Color(0xff99b2dd),
        elevation: 0.0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back,
              color: _dark ? Colors.white : Colors.black),
          onPressed: () {
            Navigator.of(context).pop();
            stopListening();
          },
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          color: _dark ? Color(0xff3a6a8c) : Color(0xff99b2dd),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                padding: EdgeInsets.only(left: size.width * 0.05),
                alignment: Alignment.centerLeft,
                child: Text(
                  "New",
                  style: Theme.of(context)
                      .textTheme
                      .headline4
                      .copyWith(color: _dark ? Colors.white : Colors.black),
                ),
              ),
              Container(
                  margin: EdgeInsets.only(top: size.height * 0.03),
                  width: size.width * 0.3,
                  alignment: Alignment.center,
                  child: this.picture == null
                      ? Image.asset(imageUrl)
                      : Image.file(this.picture)),
              Container(
                margin: EdgeInsets.only(top: size.height * 0.03),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      primary: _dark ? Color(0xff99b2dd) : Color(0xff3a6a8c)),
                  child: Text("Upload Picture"),
                  onPressed: () async {
                    uploadImage().then((File result) {
                      setState(() {
                        this.picture = result;
                      });
                    });
                  },
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 20.0),
                alignment: Alignment.centerLeft,
                margin: EdgeInsets.only(
                  top: size.height * 0.03,
                ),
                decoration: BoxDecoration(
                    border: Border(
                        top: BorderSide(color: Colors.black, width: 1.0))),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                        margin:
                            EdgeInsets.symmetric(vertical: size.height * 0.015),
                        child: StreamBuilder(
                          stream: bloc.nameOut,
                          builder: (_, snapshot) {
                            return TextField(
                              onChanged: bloc.nameChange,
                              style: TextStyle(color: Colors.black),
                              textCapitalization: TextCapitalization.words,
                              cursorColor: Colors.black,
                              decoration: InputDecoration(
                                  hintText: "Name", errorText: snapshot.error),
                            );
                          },
                        )),
                    Container(
                        margin:
                            EdgeInsets.symmetric(vertical: size.height * 0.015),
                        child: StreamBuilder(
                          stream: bloc.breedOut,
                          builder: (_, snapshot) {
                            return TextField(
                              onChanged: bloc.breedChange,
                              cursorColor: Colors.black,
                              style: TextStyle(color: Colors.black),
                              decoration: InputDecoration(
                                  hintText: "Breed", errorText: snapshot.error),
                            );
                          },
                        )),
                    Container(
                      margin:
                          EdgeInsets.symmetric(vertical: size.height * 0.015),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Flexible(
                              child: StreamBuilder(
                            stream: bloc.heightOut,
                            builder: (_, snapshot) {
                              return TextField(
                                onChanged: bloc.heightChange,
                                keyboardType: TextInputType.number,
                                cursorColor: Colors.black,
                                style: TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    hintText: "Height",
                                    errorText: snapshot.error),
                              );
                            },
                          )),
                          Padding(padding: EdgeInsets.all(5.0)),
                          Flexible(
                              child: StreamBuilder(
                            stream: bloc.weightOut,
                            builder: (_, snapshot) {
                              return TextField(
                                onChanged: bloc.weightChange,
                                keyboardType: TextInputType.number,
                                cursorColor: Colors.black,
                                style: TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    hintText: "Weight",
                                    errorText: snapshot.error),
                              );
                            },
                          )),
                          Padding(padding: EdgeInsets.all(5.0)),
                          Flexible(
                              child: StreamBuilder(
                            stream: bloc.ageOut,
                            builder: (_, snapshot) {
                              return TextField(
                                onChanged: bloc.ageChange,
                                keyboardType: TextInputType.number,
                                cursorColor: Colors.black,
                                style: TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    hintText: "Age", errorText: snapshot.error),
                              );
                            },
                          ))
                        ],
                      ),
                    ),
                    Container(
                      child: OutlinedButton(
                        child: Row(
                          children: [
                            Icon(
                              Icons.calendar_today_outlined,
                              color: _dark ? Colors.white : Colors.black,
                            ),
                            Text(
                              "${date ?? 'Pick a Birthday!'}",
                              style: TextStyle(
                                  color: _dark ? Colors.white : Colors.black),
                            ),
                          ],
                        ),
                        onPressed: () {
                          showDatePicker(
                                  context: context,
                                  initialDate: DateTime.now(),
                                  firstDate: DateTime(1990),
                                  lastDate: DateTime.now())
                              .then((DateTime date) {
                            if (date != null) {
                              setState(() {
                                this.date =
                                    "${date.day}/${date.month}/${date.year}";
                              });
                              bloc.birthdayChange(
                                  "${date.day}/${date.month}/${date.year}");
                            }
                          });
                        },
                      ),
                    )
                  ],
                ),
              ),
              Container(
                  margin: EdgeInsets.only(top: size.height * 0.05),
                  width: size.width * 0.23,
                  child: StreamBuilder(
                    stream: bloc.petSubmit,
                    builder: (streamContext, snapshot) {
                      return TextButton(
                        child: Row(
                          children: [
                            Icon(
                              Icons.save,
                              color: _dark ? Colors.white : Colors.black,
                            ),
                            Text(
                              "Save",
                              style: TextStyle(
                                  color: _dark ? Colors.white : Colors.black),
                            )
                          ],
                        ),
                        onPressed: snapshot.hasData
                            ? () async {
                                if (this.date == null) {
                                  dialog(context,
                                      message: "You have to pick a date!");
                                  return;
                                } else {
                                  bool connected = await checkConnectivity();
                                  if (connected) {
                                    try {
                                      dialog(
                                          context, content: LoadingSpinner());
                                      String petId = await bloc.createPet();
                                      _sendEvent();
                                      if (this.picture != null) {
                                        await bloc
                                            .pictureChange(
                                            [this.picture, petId]);
                                      }
                                      Navigator.pop(streamContext);
                                      showToast("Pet created", streamContext);
                                      Navigator.pop(context);
                                    } catch (e) {
                                      Navigator.pop(streamContext);
                                      showToast(e, context);
                                    }
                                  }
                                  else{
                                    _noConnectionDialog(context);
                                  }
                                }
                              }
                            : null,
                      );
                    },
                  ))
            ],
          ),
        ),
      ),
    );
  }

  _noConnectionDialog(BuildContext context) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) => AlertDialog(
        title: Text(
          "No internet connection",
          style: Theme.of(context)
              .textTheme
              .headline4
              .copyWith(color: Colors.black, fontWeight: FontWeight.bold),
        ),
        content: Text(
          'You don\'t have an internet connection, your pet was not registered. Try again later.',
          style: Theme.of(context).textTheme.headline6.copyWith(
            color: Colors.black,
          ),
        ),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text('Ok'),
          ),
        ],
      ),
    );
  }

}
