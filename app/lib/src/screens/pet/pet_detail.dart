import 'dart:async';

import 'package:app/src/utils/night_mode.dart';
import 'package:flutter/material.dart';
import 'package:light/light.dart';

import '../../models/pet.dart' as PetModel;

class PetDetailScreen extends StatefulWidget {
  final PetModel.Pet currentPet;

  PetDetailScreen(this.currentPet);

  @override
  _PetDetailScreenState createState() => _PetDetailScreenState();
}

class _PetDetailScreenState extends State<PetDetailScreen> {
  //light sensor dark mode code --------------------------------------
  Light _light;
  bool _dark = false;
  StreamSubscription _subscription;

  void onData(int luxValue) async {
    print("lux value: $luxValue");
    setState(() {
      _dark = luxValue <= 10 ? true : false;
    });
  }

  void stopListening() {
    _subscription.cancel();
  }

  void startListening() {
    _light = new Light();
    try {
      _subscription = _light.lightSensorStream.listen(onData);
    } on LightException catch (e) {
      print(e);
    }
  }

  Future<void> initPlatFormState() async {
    startListening();
  }

  //End of light sensor dark mode code ---------------------------------

  @override
  void initState() {
    super.initState();
    initPlatFormState();
  }

  @override
  Widget build(BuildContext context) {
    final String defaultPetImage = "assets/icons/scottish-fold-cat.png";
    final size = MediaQuery.of(context).size;
    bool nightMode = isNightMode();
    var petImage;

    if (widget.currentPet.imagePath.isEmpty) {
      petImage = AssetImage(defaultPetImage);
    } else {
      petImage = NetworkImage(widget.currentPet.imagePath);
    }

    return Scaffold(
      backgroundColor: _dark ? Color(0xff3a6a8c) : Color(0xff99b2dd),
      appBar: AppBar(
        backgroundColor: _dark ? Color(0xff3a6a8c) : Color(0xff99b2dd),
        elevation: 0.0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back,
              color: _dark ? Colors.white : Colors.black),
          onPressed: () {
            Navigator.of(context).pop();
            stopListening();
          },
        ),
      ),
      body: Container(
        alignment: Alignment.center,
        child: Column(
          children: [
            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: size.width * 0.05),
              margin: EdgeInsets.only(bottom: size.width * 0.05),
              child: Text(
                "${widget.currentPet.name}",
                style: Theme.of(context)
                    .textTheme
                    .headline4
                    .copyWith(color: _dark ? Colors.white : Colors.black),
              ),
            ),
            CircleAvatar(
              backgroundImage: petImage,
              maxRadius: size.width * 0.25,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.only(
                top: size.height * 0.03,
              ),
              decoration: BoxDecoration(
                  border:
                      Border(top: BorderSide(color: Colors.black, width: 1.0))),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Breed: ${widget.currentPet.breed}",
                      style: Theme.of(context).textTheme.headline6.copyWith(
                          color: _dark ? Colors.white : Colors.black)),
                  Padding(padding: EdgeInsets.all(5.0)),
                  Text("Height: ${widget.currentPet.height}",
                      style: Theme.of(context).textTheme.headline6.copyWith(
                          color: _dark ? Colors.white : Colors.black)),
                  Padding(padding: EdgeInsets.all(5.0)),
                  Text("Weight: ${widget.currentPet.weight}",
                      style: Theme.of(context).textTheme.headline6.copyWith(
                          color: _dark ? Colors.white : Colors.black)),
                  Padding(padding: EdgeInsets.all(5.0)),
                  Text("Age: ${widget.currentPet.age} years",
                      style: Theme.of(context).textTheme.headline6.copyWith(
                          color: _dark ? Colors.white : Colors.black)),
                  Padding(padding: EdgeInsets.all(5.0)),
                  Text("Birthday: ${widget.currentPet.birthday}",
                      style: Theme.of(context)
                          .textTheme
                          .headline6
                          .copyWith(color: _dark ? Colors.white : Colors.black))
                ],
              ),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: null,
        child: Icon(Icons.edit),
        backgroundColor: _dark ? Color(0xff99b2dd) : Color(0xff3a6a8c),
      ),
    );
  }
}
