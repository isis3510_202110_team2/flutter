import 'dart:async';

import 'package:app/src/bloc/bloc_provider.dart';
import 'package:app/src/bloc/blocs/user/store_vet_list_bloc.dart';
import 'package:app/src/screens/services/store_vet/store_vet_list.dart';
import 'package:app/src/utils/checkConnection.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';
import 'package:light/light.dart';

import '../../utils/night_mode.dart';
import '../../widgets/button.dart';

class ServicesScreen extends StatefulWidget {
  final FirebaseAnalytics analytics;
  final FirebaseAnalyticsObserver observer;

  ServicesScreen({this.analytics, this.observer});

  @override
  _ServicesScreenState createState() => _ServicesScreenState();
}

class _ServicesScreenState extends State<ServicesScreen> {
  //light sensor dark mode code --------------------------------------
  Light _light;
  bool _dark = false;
  StreamSubscription _subscription;

  void onData(int luxValue) async {
    print("lux value: $luxValue");
    setState(() {
      _dark = luxValue <= 10 ? true : false;
    });
  }

  void stopListening() {
    _subscription.cancel();
  }

  void startListening() {
    _light = new Light();
    try {
      _subscription = _light.lightSensorStream.listen(onData);
    } on LightException catch (e) {
      print(e);
    }
  }

  Future<void> initPlatFormState() async {
    startListening();
  }

  //End of light sensor dark mode code ---------------------------------

  Future<void> _setCurrentScreen() async {
    await widget.analytics.setCurrentScreen(screenName: "ServicesView");
  }

  Future<void> _sendEvent() async {
    await widget.analytics.logEvent(name: "services_screen", parameters: null);
  }

  //Important to start the sensor read light information
  @override
  void initState() {
    super.initState();
    initPlatFormState();
    _setCurrentScreen();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    bool nightMode = isNightMode();

    _sendEvent();
    return Scaffold(
      backgroundColor: _dark ? Colors.black : Colors.white,
      appBar: AppBar(
        backgroundColor: _dark ? Colors.black : Colors.white,
        elevation: 0.0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back,
              color: _dark ? Colors.white : Colors.black),
          onPressed: () {
            Navigator.of(context).pop();
            stopListening();
          },
        ),
      ),
      body: Container(
        child: Column(
          children: [
            Container(
              width: size.width,
              margin: EdgeInsets.only(top: 10.0),
              padding: EdgeInsets.only(left: 10.0),
              child: Text(
                "Services",
                style: Theme.of(context).textTheme.headline4.copyWith(
                      color: _dark ? Colors.white : Colors.black,
                    ),
                textAlign: TextAlign.start,
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: size.height * 0.05),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    "assets/icons/veterinary.png",
                    width: size.width * 0.4,
                  ),
                  Padding(padding: EdgeInsets.all(size.width * 0.05)),
                  Image.asset(
                    "assets/icons/pet-shop-logo.png",
                    width: size.width * 0.4,
                  )
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: size.height * 0.01),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AppButton(
                    color: Theme.of(context).colorScheme.primary,
                    text: "Vets",
                    onPressed: () {
                      checkConnectivity().then((connected) {
                        if (connected) {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (_) => Provider(
                                bloc: StoreVetListBloc(),
                                child: StoreVetListScreen(
                                  stores: false,
                                  analytics: widget.analytics,
                                  observer: widget.observer,
                                ),
                              ),
                            ),
                          );
                          stopListening();
                        } else {
                          _noConnectionDialog(context);
                        }
                      });
                    },
                    minWidth: size.width * 0.35,
                  ),
                  Padding(padding: EdgeInsets.all(size.width * 0.08)),
                  AppButton(
                    color: Theme.of(context).colorScheme.primary,
                    text: "Stores",
                    onPressed: () {
                      checkConnectivity().then((connected) {
                        if (connected) {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (_) => Provider(
                                bloc: StoreVetListBloc(),
                                child: StoreVetListScreen(
                                  stores: true,
                                  analytics: widget.analytics,
                                  observer: widget.observer,
                                ),
                              ),
                            ),
                          );
                          stopListening();
                        } else {
                          _noConnectionDialog(context);
                        }
                      });
                    },
                    minWidth: size.width * 0.35,
                  )
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: size.height * 0.01),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    "assets/icons/dog.png",
                    width: size.width * 0.4,
                  ),
                  Padding(padding: EdgeInsets.all(size.width * 0.05)),
                  Image.asset(
                    "assets/icons/dog-house-2.png",
                    width: size.width * 0.4,
                  )
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: size.height * 0.01),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AppButton(
                    color: Theme.of(context).colorScheme.primary,
                    text: "Walkers",
                    onPressed: null,
                    minWidth: size.width * 0.35,
                  ),
                  Padding(padding: EdgeInsets.all(size.width * 0.08)),
                  AppButton(
                    color: Theme.of(context).colorScheme.primary,
                    text: "Daycare",
                    onPressed: null,
                    minWidth: size.width * 0.35,
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  _noConnectionDialog(BuildContext context) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) => AlertDialog(
        title: Text(
          "No internet connection",
          style: Theme.of(context)
              .textTheme
              .headline4
              .copyWith(color: Colors.black, fontWeight: FontWeight.bold),
        ),
        content: Text(
          'You don\'t have an internet connection, try again later.',
          style: Theme.of(context).textTheme.headline6.copyWith(
                color: Colors.black,
              ),
        ),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text('Ok'),
          ),
        ],
      ),
    );
  }
}
