import 'dart:async';

import 'package:app/src/bloc/bloc_provider.dart';
import 'package:app/src/bloc/blocs/update_profile_picture_bloc.dart';
import 'package:app/src/bloc/blocs/user/store_vet_creation_bloc.dart';
import 'package:app/src/screens/services/store_vet/add_marker.dart';
import 'package:app/src/screens/services/store_vet/add_office_hours.dart';
import 'package:app/src/screens/user/profile_picture_upload.dart';
import 'package:app/src/utils/notification_dialog.dart';
import 'package:app/src/widgets/button.dart';
import 'package:app/src/widgets/spinner.dart';
import 'package:app/src/widgets/toast_alert.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';
import 'package:light/light.dart';

class StoreVetCreationScreen extends StatefulWidget {
  final String vetId;

  final FirebaseAnalytics analytics;
  final FirebaseAnalyticsObserver observer;

  StoreVetCreationScreen({this.vetId, this.analytics, this.observer});
  @override
  _StoreVetCreationScreenState createState() => _StoreVetCreationScreenState();
}

class _StoreVetCreationScreenState extends State<StoreVetCreationScreen> {
  //light sensor dark mode code --------------------------------------
  Light _light;
  bool _dark = false;
  StreamSubscription _subscription;

  void onData(int luxValue) async {
    print("lux value: $luxValue");
    setState(() {
      _dark = luxValue <= 10 ? true : false;
    });
  }

  void stopListening() {
    _subscription.cancel();
  }

  void startListening() {
    _light = new Light();
    try {
      _subscription = _light.lightSensorStream.listen(onData);
    } on LightException catch (e) {
      print(e);
    }
  }

  Future<void> initPlatFormState() async {
    startListening();
  }

  //End of light sensor dark mode code ---------------------------------

  List<Map<String, double>> locations = [];
  Map<String, List<String>> officeHours = {};

  bool storeRole = false;

  Future<void> _setCurrentScreen() async {
    await widget.analytics.setCurrentScreen(screenName: "ServiceCreation");
  }

  Future<void> _logService() async {
    await widget.analytics.logEvent(
        name: "service_register", parameters: {'Store': this.storeRole});
  }

//Important to start the sensor read light information
  @override
  void initState() {
    super.initState();
    initPlatFormState();
    _setCurrentScreen();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final bloc = Provider.of<StoreVetCreationBloc>(context);
    return Scaffold(
      backgroundColor: _dark ? Colors.black : Colors.white,
      appBar: AppBar(
        backgroundColor: _dark ? Colors.black : Colors.white,
        elevation: 0.0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back,
              color: _dark ? Colors.white : Colors.black),
          onPressed: () {
            Navigator.of(context).pop();
            stopListening();
          },
        ),
      ),
      body: Container(
        alignment: Alignment.center,
        child: Column(
          children: [
            Container(
              child: Text(
                "Register",
                style: Theme.of(context)
                    .textTheme
                    .headline4
                    .copyWith(color: _dark ? Colors.white : Colors.black),
              ),
              margin: EdgeInsets.only(bottom: size.height * 0.05),
            ),
            Container(
              margin: EdgeInsets.only(bottom: size.height * 0.05),
              width: size.width * 0.8,
              child: AppButton(
                color: Theme.of(context).colorScheme.secondary,
                onPressed: () async {
                  var result = await Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (_) => AddMarkerScreen(
                                locations: this.locations,
                              )));
                  setState(() {
                    if (result != null) {
                      this.locations = result;
                    }
                  });
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Icon(
                      Icons.gps_fixed,
                      color: Colors.black,
                    ),
                    Text("Pick Your Locations")
                  ],
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: size.height * 0.05),
              width: size.width * 0.8,
              child: AppButton(
                color: Theme.of(context).colorScheme.secondary,
                onPressed: () async {
                  var result = await Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (_) => AddOfficeHoursScreen(
                                officeHours: this.officeHours,
                              )));
                  setState(() {
                    if (result != null) {
                      this.officeHours = result;
                    }
                  });
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Icon(
                      Icons.calendar_today,
                      color: Colors.black,
                    ),
                    Text("Pick Office Hours")
                  ],
                ),
              ),
            ),
            Container(
              width: size.width * 0.8,
              child: AppButton(
                color: Theme.of(context).colorScheme.secondary,
                onPressed: null,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Icon(
                      Icons.map,
                      color: Colors.black,
                    ),
                    Text("Upload Catalog")
                  ],
                ),
              ),
            ),
            Container(
                margin: EdgeInsets.only(bottom: size.height * 0.05),
                width: size.width * 0.8,
                child: Row(
                  children: [
                    Text(
                      "Are you a store provider?",
                      style: Theme.of(context).textTheme.bodyText1.copyWith(
                          color: Theme.of(context).colorScheme.primary),
                    ),
                    Checkbox(
                      fillColor: _dark
                          ? MaterialStateProperty.all(Colors.white)
                          : MaterialStateProperty.all(Colors.black),
                      hoverColor: Colors.white,
                      activeColor: Colors.white,
                      checkColor: Color(0xff3a6a8c),
                      onChanged: (bool value) {
                        setState(() {
                          this.storeRole = value;
                        });
                      },
                      value: this.storeRole,
                    ),
                  ],
                )),
            Container(
              margin: EdgeInsets.only(bottom: size.height * 0.05),
              width: size.width * 0.8,
              child: AppButton(
                color: Theme.of(context).colorScheme.primary,
                onPressed: () async {
                  _logService();
                  bloc.roleChange(storeRole);
                  bloc.locationsChange(locations);
                  bloc.officeHoursChange(officeHours);
                  dialog(context, content: LoadingSpinner());
                  String userId;
                  try {
                    userId = await bloc.submit();
                  } catch (e) {
                    Navigator.pop(context);
                    showToast(e, context);
                    return;
                  }
                  Navigator.pop(context);
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (_) => Provider(
                              bloc: UpdateProfilePictureBloc(),
                              child: ProfilePictureUploadScreen(userId,
                                  directory: "user_pictures/store_vet"))));
                  stopListening();
                },
                text: "Next",
              ),
            )
          ],
        ),
      ),
    );
  }
}
