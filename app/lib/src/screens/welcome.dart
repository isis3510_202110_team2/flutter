import 'package:app/src/utils/night_mode.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';

import '../utils/checkConnection.dart';
import '../widgets/button.dart';

class WelcomeScreen extends StatelessWidget {
  final FirebaseAnalytics analytics;
  final FirebaseAnalyticsObserver observer;

  WelcomeScreen({this.analytics, this.observer});

  Future<void> _setCurrentScreen() async {
    await analytics.setCurrentScreen(screenName: "Welcome");
  }

  @override
  Widget build(BuildContext context) {
    bool nightMode = isNightMode();
    final size = MediaQuery.of(context).size;
    _setCurrentScreen();
    return Container(
      color: Theme.of(context).colorScheme.primaryVariant,
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.all(30.0),
          ),
          Text("Togo",
              style: Theme.of(context)
                  .textTheme
                  .headline4
                  .copyWith(color: Colors.black, fontWeight: FontWeight.bold)),
          Padding(padding: EdgeInsets.all(30.0)),
          Image.asset(
            "assets/icons/app-icon.png",
            width: size.width,
          ),
          Expanded(
            child: Align(
              alignment: Alignment.bottomCenter,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AppButton(
                    color: Theme.of(context).colorScheme.primary,
                    text: "Login",
                    onPressed: () {
                      checkConnectivity().then((connected) {
                        if (connected) {
                          Navigator.pushNamed(context, "/login");
                        } else {
                          showDialog(
                            barrierDismissible: false,
                            context: context,
                            builder: (context) => AlertDialog(
                              title: Text(
                                "No internet connection",
                                style: Theme.of(context)
                                    .textTheme
                                    .headline4
                                    .copyWith(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold),
                              ),
                              content: RichText(
                                text: new TextSpan(
                                  // Note: Styles for TextSpans must be explicitly defined.
                                  // Child text spans will inherit styles from parent
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline6
                                        .copyWith(color: Colors.black),
                                    children: <TextSpan>[
                                      new TextSpan(text: 'You don\'t have an internet connection,'),
                                      new TextSpan(text: ' login will only be made if it matches the credentials from the last online session made.', style: new TextStyle(fontWeight: FontWeight.bold)),
                                    ]
                                ),
                              ),
                              actions: <Widget>[
                                TextButton(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                    Navigator.pushNamed(context, "/login");
                                  },
                                  child: Text('Continue'),
                                ),
                                TextButton(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                  child: Text('Cancel'),
                                ),
                              ],
                            ),
                          );
                        }
                      });
                    },
                    minWidth: size.width * 0.45,
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 10.0),
                  ),
                  AppButton(
                    color: Theme.of(context).colorScheme.secondary,
                    text: "Register",
                    onPressed: () {
                      checkConnectivity().then((connected) {
                        if (!connected) {
                          showDialog(
                            barrierDismissible: false,
                            context: context,
                            builder: (context) => AlertDialog(
                              title: Text(
                                "No internet connection",
                                style: Theme.of(context)
                                    .textTheme
                                    .headline4
                                    .copyWith(
                                        color: nightMode
                                            ? Colors.white
                                            : Colors.black,
                                        fontWeight: FontWeight.bold),
                              ),
                              content: RichText(
                                text: new TextSpan(
                                  // Note: Styles for TextSpans must be explicitly defined.
                                  // Child text spans will inherit styles from parent
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline6
                                        .copyWith(color: Colors.black),
                                    children: <TextSpan>[
                                      new TextSpan(text: 'Keep in mind you don\'t have an internet connection at the moment, you may fill the form and'),
                                      new TextSpan(text: ' credentials will be saved locally for the next time connection returns.', style: new TextStyle(fontWeight: FontWeight.bold)),
                                    ]
                                ),
                              ),
                              actions: <Widget>[
                                TextButton(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                    Navigator.pushNamed(context, "/register");
                                  },
                                  child: Text('Continue'),
                                ),
                                TextButton(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                  child: Text('Cancel'),
                                ),
                              ],
                            ),
                          );
                        } else {
                          Navigator.pushNamed(context, "/register");
                        }
                      });
                    },
                    minWidth: MediaQuery.of(context).size.width * 0.45,
                  )
                ],
              ),
            ),
          ),
          SizedBox( height: 20,)
        ],
      ),
    );
  }
}
