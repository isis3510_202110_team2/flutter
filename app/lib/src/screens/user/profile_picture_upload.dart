import 'dart:async';
import 'dart:io';

import 'package:app/src/screens/user/home.dart';
import 'package:app/src/utils/checkConnection.dart';
import 'package:app/src/widgets/toast_alert.dart';
import 'package:flutter/material.dart';
import 'package:light/light.dart';

import '../../bloc/bloc_provider.dart';
import '../../bloc/blocs/update_profile_picture_bloc.dart';
import '../../utils/notification_dialog.dart';
import '../../utils/permissions.dart';
import '../../widgets/button.dart';
import '../../widgets/spinner.dart';

class ProfilePictureUploadScreen extends StatefulWidget {
  final String userId;
  final String directory;

  ProfilePictureUploadScreen(this.userId,
      {this.directory = "user_pictures/pet_owner"});

  @override
  _ProfilePictureUploadScreenState createState() =>
      _ProfilePictureUploadScreenState();
}

class _ProfilePictureUploadScreenState
    extends State<ProfilePictureUploadScreen> {
  //light sensor dark mode code --------------------------------------
  Light _light;
  bool _dark = false;
  StreamSubscription _subscription;

  void onData(int luxValue) async {
    print("lux value: $luxValue");
    setState(() {
      _dark = luxValue <= 10 ? true : false;
    });
  }

  void stopListening() {
    _subscription.cancel();
  }

  void startListening() {
    _light = new Light();
    try {
      _subscription = _light.lightSensorStream.listen(onData);
    } on LightException catch (e) {
      print(e);
    }
  }

  Future<void> initPlatFormState() async {
    startListening();
  }

  //End of light sensor dark mode code ---------------------------------

  File picture;
  String filename;
  String nextButtonText = "Next";
  bool allowed = false;

  //Important to start the sensor read light information
  @override
  void initState() {
    super.initState();
    initPlatFormState();
  }

  @override
  Widget build(BuildContext context) {
    final String userId = widget.userId;

    final size = MediaQuery.of(context).size;
    final String imageUrl = "assets/icons/profile.png";
    final bloc = Provider.of<UpdateProfilePictureBloc>(context);

    return Scaffold(
      backgroundColor: _dark ? Colors.black : Colors.white,
      appBar: AppBar(
        backgroundColor: _dark ? Colors.black : Colors.white,
        elevation: 0.0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back,
              color: _dark ? Colors.white : Colors.black),
          onPressed: () {
            Navigator.of(context).pop();
            stopListening();
          },
        ),
      ),
      body: Container(
        width: size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              margin: EdgeInsets.only(top: size.height * 0.1),
              width: size.width * 0.8,
              alignment: Alignment.center,
              child: Card(
                elevation: 2.0,
                child: this.picture == null
                    ? Image.asset(imageUrl)
                    : Image.file(this.picture),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: size.height * 0.02),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    margin: EdgeInsets.only(right: size.height * 0.01),
                    child: AppButton(
                      color: Theme.of(context).colorScheme.primary,
                      onPressed: () {
                        uploadImage().then((File result) {
                          setState(() {
                            this.picture = result;
                            this.filename = userId +
                                "." +
                                this
                                    .picture
                                    .path
                                    .split("/")
                                    .last
                                    .split(".")
                                    .last;
                            bloc.profileImageChange(
                                [this.filename, this.picture]);
                            allowed = true;
                            nextButtonText = "Next";
                          });
                        }).catchError((error) {
                          showToast(error, context);
                          setState(() {
                            nextButtonText = "Jump";
                            allowed = true;
                          });
                        });
                      },
                      text: "Upload",
                    ),
                  ),
                  Container(
                      child: StreamBuilder(
                    stream: bloc.profileImageOut,
                    builder: (streamContext, snapshot) {
                      return AppButton(
                        color: Theme.of(context).colorScheme.primary,
                        text: nextButtonText,
                        onPressed: allowed
                            ? () async {
                          bool connected = await checkConnectivity();
                          if (connected) {
                            dialog(context, content: LoadingSpinner());
                            var streamList = snapshot.data;

                            try {
                              await bloc.upload(
                                  streamList[0], streamList[1],
                                  directory: widget.directory);
                              Navigator.pop(streamContext);
                            } catch (error) {
                              Navigator.pop(streamContext);
                            }

                            showToast(
                                "User created successfully!", context);
                            Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (_) => HomeScreen()));
                            stopListening();
                          }
                          else{
                            _noConnectionDialog(context);
                          }
                        }
                            : null,
                      );
                    },
                  ))
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
  _noConnectionDialog(BuildContext context) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) => AlertDialog(
        title: Text(
          "No internet connection",
          style: Theme.of(context)
              .textTheme
              .headline4
              .copyWith(color: Colors.black, fontWeight: FontWeight.bold),
        ),
        content: Text(
          'You don\'t have an internet connection, your photo cannot be uploaded. You may change it later.',
          style: Theme.of(context).textTheme.headline6.copyWith(
            color: Colors.black,
          ),
        ),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
              showToast(
                  "User created without profile photo!", context);
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                      builder: (_) => HomeScreen()));
              stopListening();
            },
            child: Text('Ok'),
          ),
        ],
      ),
    );
  }
}
